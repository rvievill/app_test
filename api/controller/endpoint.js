const express = require("express");
const app = express();

app.use(express.urlencoded());

app.post("/", (req, res) => {
	res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Headers", "Content-Type");
  console.log(JSON.parse(req.body.data));
  res.send(req.body.data);
});

app.listen(8080, () => {
  console.log("server on");
});
