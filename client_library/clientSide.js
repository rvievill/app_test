const post = function () {
  const data = {
    name: "Bassel Hamadeh",
    date_of_birth: "1984-10-14",
    documents: [
      {
        title: "Some title",
        href: "https://s3.aws.com/somefile.",
      },
      {
        title: "Some title",
        href: "https://s3.aws.com/somefile.",
      }
    ]
  };

  const xhr = new XMLHttpRequest();
  xhr.open("POST", "http://localhost:8080/");
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  console.log(JSON.stringify(data));
  xhr.send(`data=${JSON.stringify(data)}`);
};